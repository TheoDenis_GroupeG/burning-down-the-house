from random import *
from tkinter import *
import argparse

color_plain = "#FFFFFF"
color_void = "#2C3E50"
color_ashes = "#7F8C8D"
color_fire = "#C0392B"
color_tree = "#6B8E23"


def otamatonegen(rows = 10, cols = 10, proba = 0.6):
    otamatone = []
    for row in range(rows):
        row = []
        for col in range(cols):
            if random() < proba:
                row.append("tree")
            else:
                row.append("void")    
        otamatone.append(row)
    return otamatone


def burningneighbors(otamatone, rows, cols, row, col):
    count = 0
    if row > 0 and otamatone[row-1][col] == "fire":
        count += 1
    if row < rows-1 and otamatone[row+1][col] == "fire":
        count += 1
    if col > 0 and otamatone[row][col-1] == "fire":
        count += 1
    if col < cols-1 and otamatone[row][col+1] == "fire":
        count += 1
    return count


def nextstageCell(otamatone, rows, cols, row, col):
    if otamatone[row][col] == "void" or otamatone[row][col] == "ashes":
        return "void"
    elif otamatone[row][col] == "fire" :
        return "ashes"
    elif rule_button.cget("text") == "Rule n°1" and burningneighbors(otamatone, rows, cols, row, col) > 0:
        return "fire"
    elif  1-(1/(burningneighbors(otamatone, rows, cols, row, col)+1)) > random():
        return "fire"
    else:
        return "tree"


def nextstage(otamatone, rows, cols):
    nextstage = []
    for row in range(rows):
        row = []
        for col in range(cols):
            row.append(0)
        nextstage.append(row)

    for row in range(rows):
        for col in range(cols):
            nextstage[row][col] = nextstageCell(otamatone, rows, cols, row, col)
    
    for row in range(rows):
        for col in range(cols):
            otamatone[row][col] = nextstage[row][col]


# Simulation


def simstart(generation):
    start_button.config(text = "Generation #%d" % generation) 
    nextstage(simulation, args.rows, args.cols)
    generation += 1
    updatesimulation(simulation, args.rows, args.cols)
    master.after(int(args.speed*1000), simstart, generation)


# Canvas Creation


def simdraw(rows, cols, size):
    for row in range(rows):
        for col in range(cols):
            cnvs_simulation.create_rectangle(col*size, row*size, (col*size)+size, (row*size)+size, fill = color_plain, tags = "%d,%d"% (row, col))


# Canvas Update


def updatesimulation(otamatone, rows, cols):
    for row in range(rows):
        for col in range(cols):
            if otamatone[row][col] == "tree" :
                new_color = color_tree
            elif otamatone[row][col] == "ashes" :
                new_color = color_ashes
            elif otamatone[row][col] == "fire" :
                new_color = color_fire
            else: 
                new_color = color_plain
            cnvs_simulation.itemconfigure("%d,%d"%(row, col), fill = new_color)


# Callback Functions


def callback_clickcnvs(event):
    col = int(event.x / args.cell_size)
    row = int(event.y / args.cell_size)
    if simulation[row][col] == "tree":
        simulation[row][col] = "fire"
    elif simulation[row][col] == "fire":
        simulation[row][col] = "tree"
    updatesimulation(simulation, args.rows, args.cols)


def callback_start():
    start_button.config(state = DISABLED)
    rule_button.config(state = DISABLED)
    simstart(0)


def callback_Rule():
    if rule_button.cget("text") == "Rule n°1":
        rule_button.config(text = "Rule n°2")
    else:
        rule_button.config(text = "Rule n°1")

    
# Main


if __name__ == '__main__':
    # Argumets/Options
    parser = argparse.ArgumentParser(description = "forest fire simulation")
    parser.add_argument("-rows", help = "rows in the forest map", type = int, default = 10)
    parser.add_argument("-cols", help = "columns in the forest map", type = int, default = 10)
    parser.add_argument("-anim", help = "animation speed rate", type = float, default = 0.5)
    parser.add_argument("-size", help = "cells size (pixels)", type = int, default = 24)
    parser.add_argument("-afforestation", help = "afforestation ratio", type = float, default = 0.6)
    args = parser.parse_args()

    # UI
    master = Tk()
    master.title("Simulation")
    master.resizable(width = False, height = False)
    
    master.rowconfigure(0, weight = 1)
    master.rowconfigure(1, weight = 5)
    master.columnconfigure(0, weight = 1)
    master.columnconfigure(1, weight = 1)

    start_button = Button(master,
                             compound = "left",
                             text = 'Start',
                             command = callback_start)
    start_button.grid(row = 0, column = 0)

    rule_button = Button(master,
                             compound = "left",
                             text = 'Rule n°1',
                             command = callback_Rule)
    rule_button.grid(row = 0, column = 1)

    cnvs_simulation = Canvas(master,
                                bg = color_plain,
                                width = (args.cell_size*args.cols),
                                height = (args.cell_size*args.rows))
    cnvs_simulation.grid(row = 1, column = 0, columnspan = 2)
    cnvs_simulation.bind('<Button-1>', callback_clickcnvs)

    simulation = otamatonegen(args.rows, args.cols, args.afforestation)
    simdraw(args.rows, args.cols, args.cell_size)
    updatesimulation(simulation, args.rows, args.cols)
    master.mainloop()